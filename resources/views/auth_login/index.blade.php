@extends('layouts.app-layout')

@section('title', 'Register')

@section('content')
  

  <br>
  <br>
  <div class="container">
    <!-- Content here -->
    <div class="row justify-content-center">
      <div class="col-lg-4">
        <div class="card">
          <div class="card-body login-card-body">
            <h2 class=""> Log In</h2>
            <p class="login-box-msg">Don't have account? <a href="#"> Create account</a></p>
            <form action="" method="post">
              @csrf
              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" class="form-control">
              </div>
              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control">
              </div>


              <div class="row">
                <div class="col-8">
                </div>
                <!-- /.col -->
                <div class="col-4">
                  <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                </div>
                <!-- /.col -->
              </div>
            </form>
            <br>
            <p class="mb-0">
              <a href="{{ route('register') }}" class="text-center">Register a new user</a>
            </p>
          </div>
          <!-- /.login-card-body -->
        </div>

      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div>

@endsection
