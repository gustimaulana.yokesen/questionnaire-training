@extends('layouts.main')

@section('title', 'Questionnaire')

@section('content')

  <br>
  <br>
  <br>
  <br>
  <br>
  <div class="container">
    <!-- Content here -->
    <div class="align-content-center">
      <div class="row">
        <div class="col-lg-12">
          <h1 style="text-align:center; font-family: 'Readex Pro', sans-serif;  font-size: 96px;">Questionnaire</h1>
          <p style="text-align:center; font-family: 'Readex Pro', normal; font-weight: 500; font-size: 24px; line-height: 30px; color: rgba(0, 0, 0, 0.5);">Lorem</p>
          <div class="row">
            <div class="col-2">
              <p>56%</p>
            </div>
            <div class="col-10">
              <div class="progress">
                <div class="progress-bar w-50" role="progressbar" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- list ques --}}
  <div class="container">
    <div class="align-content-center">
      <div class="row">
        <div class="col-md-12">
          <table>
            <tr>
              <td>Saya tidak ramah</td>
              <td>
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
              </td>
            </tr>
            <tr>
              <td>Saya tidak ramah</td>
              <td>
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
              </td>
            </tr>
            <tr>
              <td>Saya tidak ramah</td>
              <td>
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
              </td>
            </tr>
            <tr>
              <td>Saya tidak ramah</td>
              <td>
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
              </td>
            </tr>
            <tr>
              <td>Saya tidak ramah</td>
              <td>
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
              </td>
            </tr>
            <tr>
              <td>Saya tidak ramah</td>
              <td>
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
                  <input type="radio" name="answer[]" value="">
              </td>
            </tr>
          </table>
        </div>
      </div>
      {{-- <div class="row">
        <div class="col-lg-4">
          <p>Saya tidak ramah</p>
          <p>Saya tidak ramah</p>
          <p>Saya tidak ramah</p>
          <p>Saya tidak ramah</p>
          <p>Saya tidak ramah</p>
          <p>Saya tidak ramah</p>
          <p>Saya tidak ramah</p>
        </div>
        <div class="col-lg-8">
          <div class="form-group">
            <input type="radio" name="answer[]" value="" class="form-control">
          </div>
          <div class="form-group">
            <input type="radio" name="answer[]" value="" class="form-control">
          </div>
          <div class="form-group">
            <input type="radio" name="answer[]" value="" class="form-control">
          </div>
          <div class="form-group">
            <input type="radio" name="answer[]" value="" class="form-control">
          </div>
          <div class="form-group">
            <input type="radio" name="answer[]" value="" class="form-control">
          </div>
          <div class="form-group">
            <input type="radio" name="answer[]" value="" class="form-control">
          </div>
          <div class="form-group">
            <input type="radio" name="answer[]" value="" class="form-control">
          </div>
        </div>
      </div> --}}
      <br>
      <button type="button" class="btn" data-toggle="button" style="background-color: #FF6C1E; color: white; height: 66px; width: 177px; left: 631px; top: 1379px; border-radius: 40px; padding: 18px, 60px, 18px, 60px; font-size: 18px;">
        Next
      </button>


    </div>
  </div>

@endsection
