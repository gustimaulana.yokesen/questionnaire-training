<!doctype html>
<html x-data="data()" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->

    @include('includes.meta')

    @stack('before-style')

    @include('includes.style')

    @stack('after-style')

  </head>
  <body>
    <title>@yield('title')</title>


    @yield('content')


    @stack('before-script')

    @include('includes.script')

    @stack('after-script')
  </body>
</html>
