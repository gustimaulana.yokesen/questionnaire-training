<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h2 style="font-family: 'Readex Pro', normal;  font-size: 48px;"> Register</h2>
        <p class="login-box-msg" style="font-family: 'Readex Pro', normal;  font-size: 14px; color: rgba(0, 0, 0, 0.4);">Already have account? <a href="#" style="color: #FF6C1E;"> Log In</a></p>
        <form action="" method="post">
          @csrf
          <div class="form-group">
            <label for="email" style="font-family: 'Readex Pro', normal;  font-size: 14px; color: rgba(0, 0, 0, 0.7);">Email</label>
            <input type="email" name="email" class="form-control">
          </div>
          <div class="form-group">
            <label for="password"  style="font-family: 'Readex Pro', normal;  font-size: 14px; color: rgba(0, 0, 0, 0.7);">Password</label>
            <input type="password" name="password" class="form-control">
          </div>
          <div class="form-group">
            <label for="password_confirmation"  style="font-family: 'Readex Pro', normal;  font-size: 14px; color: rgba(0, 0, 0, 0.7);">Password</label>
            <input type="password" name="password_confirmation" class="form-control">
          </div>
          <div class="row justify-content-center">
            <div class="col-md-4">
              <button type="submit" class="btn" style="background-color: #FF6C1E; color: white;">Register</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
