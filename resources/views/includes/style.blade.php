<link rel="stylesheet" href="{{ url('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css')}}" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="preconnect" href="{{ url('https://fonts.googleapis.com')}}">
<link rel="preconnect" href="{{ url('https://fonts.gstatic.com')}}" crossorigin>
<link href="{{ url('https://fonts.googleapis.com/css2?family=Readex+Pro:wght@700&display=swap')}}" rel="stylesheet">
