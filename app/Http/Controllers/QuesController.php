<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuesController extends Controller
{
  public function index()
  {
      return view('ques.index');
  }
  public function result()
  {
      return view('result.index');
  }
}
